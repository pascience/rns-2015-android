package com.insarennes.rnsapp.view;

import android.content.res.Resources;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.model.Link;

public class LinkListViewHelper {

    protected Resources mResources;

    public LinkListViewHelper(Resources resources) {
        this.mResources = resources;
    }

    public void addLinks(LayoutInflater inflater, ViewGroup layout, Iterable<Link> links) {
        for (Link link : links) {
            addLink(inflater, layout, link);
        }
    }

    public void addLink(LayoutInflater inflater, ViewGroup list, Link link) {
        View linkView = inflater.inflate(R.layout.adapter_link, list, false);

        ImageView icon = (ImageView) linkView.findViewById(R.id.link_icon);
        TextView url = (TextView) linkView.findViewById(R.id.link_url);

        icon.setImageResource(getIcon(link));
        url.setMovementMethod(LinkMovementMethod.getInstance());

        if (link.getType() == Link.LinkType.EMAIL) {
            url.setText(Html.fromHtml("<a href=\"mailto:" + link.getUrl() + "\">" + getText(link) + "</a> "));
        } else {
            url.setText(Html.fromHtml("<a href=\"http://" + link.getUrl() + "\">" + getText(link) + "</a> "));
        }

        list.addView(linkView);
    }

    protected int getIcon(Link link) {
        switch (link.getType()) {
            case OFFICIAL:
                return R.drawable.website;
            case YOUTUBE:
                return R.drawable.youtube;
            case TWITTER:
                return R.drawable.twitter;
            case FACEBOOK:
                return R.drawable.facebook;
            case SOUNDCLOUD:
                return R.drawable.soundcloud;
            case DEEZER:
                return R.drawable.deezer;
            case SPOTIFY:
                return R.drawable.spotify;
            case EMAIL:
                return R.drawable.email;
            case FORUM:
                return R.drawable.forum;
            default:
                return R.drawable.website;
        }
    }

    protected String getText(Link link) {
        switch (link.getType()) {
            case OFFICIAL:
                return mResources.getString(R.string.officialWebSite);
            case YOUTUBE:
                return mResources.getString(R.string.youtube);
            case TWITTER:
                return mResources.getString(R.string.twitter);
            case FACEBOOK:
                return mResources.getString(R.string.facebook);
            case SOUNDCLOUD:
                return mResources.getString(R.string.soundCloud);
            case DEEZER:
                return mResources.getString(R.string.deezer);
            case SPOTIFY:
                return mResources.getString(R.string.spotify);
            case EMAIL:
                return link.getUrl();
            case FORUM:
                return mResources.getString(R.string.forum);
            default:
                return mResources.getString(R.string.officialWebSite);
        }
    }

}
