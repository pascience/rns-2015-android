package com.insarennes.rnsapp.model;

import java.util.Calendar;

public class Race {

    protected int id;
    protected String name;
    protected Calendar date;
    protected int mapResId;
    protected String infos;

    public Race(int id, String name, Calendar date, String infos) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.infos = infos;
    }

    public Race(int id, String name, Calendar date, String infos, int mapResId) {
        this(id, name, date, infos);
        this.mapResId = mapResId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getMapResId() {
        return mapResId;
    }

    public Calendar getDate() {
        return date;
    }

    public String getInfos() {
        return infos;
    }
}
