package com.insarennes.rnsapp.model;

import java.util.ArrayList;

public class Bands {

    protected ArrayList<Band> bands;

    public Bands() {
        bands = new ArrayList<>();
    }

    public Iterable<Band> getBands() {
        return bands;
    }

    public int getSize() {
        return bands.size();
    }

    public Band getBand(int index) {
        return bands.get(index);
    }

    public Band getBandFromId(int id) {
        for (Band g : getBands()) {
            if (g.getId() == id) {
                return g;
            }
        }
        return null;
    }

    public void addBand(Band band) {
        this.bands.add(band);
    }
}
