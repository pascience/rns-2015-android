package com.insarennes.rnsapp.model;

import java.util.Calendar;

public class LineUpItem {

    protected Band band;
    protected Stage stage;
    protected Calendar date;
    protected Calendar day;

    public LineUpItem(Band band, Stage stage, Calendar date) {
        this.band = band;
        this.stage = stage;
        this.date = date;
        this.day = getDayFromDate(date);
    }

    public Band getBand() {
        return band;
    }

    public void setBand(Band band) {
        this.band = band;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Calendar getDay() {
        return day;
    }

    public void setDay(Calendar day) {
        this.day = day;
    }

    public static Calendar getDayFromDate(Calendar date) {
        Calendar day = (Calendar)date.clone();
        if (day.get(Calendar.HOUR_OF_DAY) < 4) {
            day.set(Calendar.DAY_OF_MONTH, day.get(Calendar.DAY_OF_MONTH) - 1);
        }
        return day;
    }

    @Override
    public String toString() {
        return getBand().getName() + " - " + getStage().getName() + " - " + getDate();
    }
}
