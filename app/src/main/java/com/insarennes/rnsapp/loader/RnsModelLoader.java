package com.insarennes.rnsapp.loader;

import android.content.res.Resources;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.model.Band;
import com.insarennes.rnsapp.model.Bands;
import com.insarennes.rnsapp.model.LineUp;
import com.insarennes.rnsapp.model.LineUpItem;
import com.insarennes.rnsapp.model.Race;
import com.insarennes.rnsapp.model.Races;
import com.insarennes.rnsapp.model.RnsModel;
import com.insarennes.rnsapp.model.Stage;

import java.util.Calendar;

/**
 * Load data in a RnsModel object
 */
public class RnsModelLoader {

    public static void loadData(RnsModel model, Resources resources) {

        loadRock(model, resources);
        loadSolex(model, resources);
    }

    private static void loadRock(RnsModel model, Resources resources) {

        Bands bands = model.getBands();
        LineUp lineUp = model.getLineUp();

        int i = 0;

        // Main stage

        Stage mainStage = new Stage(resources.getString(R.string.mainStage));
        mainStage.setScore(resources.getInteger(R.integer.mainStageScore));
        model.setMainStage(mainStage);

        Band anthonyB = new Band(i++, resources.getString(R.string.anthonyB), resources.getString(R.string.anthonyBDescription), resources.getString(R.string.reggae), resources.getString(R.string.jamaique), R.drawable.artist_anthony_b);
        anthonyB.addLink("www.facebook.com/pages/Anthony-B/112304168785068");
        anthonyB.addLink("www.anthonybmusic.net/");
        anthonyB.addLink("www.twitter.com/anthonybreggae");
        anthonyB.addLink("www.deezer.com/artist/7061");
        anthonyB.addLink("play.spotify.com/artist/7Lij2ZLJJQOfGojVR3Wmqa");
        anthonyB.setHeadliningScrore(resources.getInteger(R.integer.anthonyBHeadliningScrore));
        bands.addBand(anthonyB);
        lineUp.addLineUpItem(new LineUpItem(anthonyB, mainStage, getDay(15, 1, 00)));

        Band agoria = new Band(i++, resources.getString(R.string.agoriaB2BCarlCraig), resources.getString(R.string.agoriaB2BCarlCraigDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_agoria);
        agoria.addLink("www.carlcraig.net");
        agoria.addLink("www.facebook.com/agoriaofficial");
        agoria.addLink("www.facebook.com/carlcraigofficial");
        agoria.addLink("www.twitter.com/carlcraignet");
        agoria.setHeadliningScrore(resources.getInteger(R.integer.agoriaHeadliningScrore));
        bands.addBand(agoria);
        lineUp.addLineUpItem(new LineUpItem(agoria, mainStage, getDay(17, 0, 00)));

        Band bigaRanx = new Band(i++, resources.getString(R.string.bigaRanx), resources.getString(R.string.bigaRanxDescription), resources.getString(R.string.dubstep), resources.getString(R.string.france), R.drawable.artist_biga_ranx);
        bigaRanx.addLink("www.bigaranxofficial.com");
        bigaRanx.addLink("www.facebook.com/bigaranxofficial");
        bigaRanx.addLink("www.twitter.com/BIGA_RANX");
        bigaRanx.addLink("www.youtube.com/channel/UCMT991TS9jLyNuxWa9f3fXg");
        bigaRanx.addLink("www.soundcloud.com/biga-ranx-1");
        bigaRanx.addLink("www.deezer.com/artist/1377312");
        bigaRanx.addLink("play.spotify.com/artist/5rgdw8NVhnxWL8Poo6HsiX");
        bigaRanx.setHeadliningScrore(resources.getInteger(R.integer.bigaRanxHeadliningScrore));
        bands.addBand(bigaRanx);
        lineUp.addLineUpItem(new LineUpItem(bigaRanx, mainStage, getDay(15, 2, 30)));

        Band dustyKid = new Band(i++, resources.getString(R.string.dustyKid), resources.getString(R.string.dustyKidDescription), resources.getString(R.string.dubstep), resources.getString(R.string.italy), R.drawable.artist_dustykid);
        dustyKid.addLink("www.dustykid.it/");
        dustyKid.addLink("www.facebook.com/DustylittleKid");
        dustyKid.addLink("www.soundcloud.com/dusty-kid-official/");
        dustyKid.addLink("www.deezer.com/artist/54227");
        dustyKid.addLink("play.spotify.com/artist/6JGLndnzrQrIFkd8NFha3C");
        dustyKid.setHeadliningScrore(resources.getInteger(R.integer.dustyKidHeadliningScrore));
        bands.addBand(dustyKid);
        lineUp.addLineUpItem(new LineUpItem(dustyKid, mainStage, getDay(16, 22, 40)));

        Band dirtyphonics = new Band(i++, resources.getString(R.string.dirtyphonics), resources.getString(R.string.dirtyphonicsDescription), resources.getString(R.string.dubstep), resources.getString(R.string.france), R.drawable.artist_dirtyphonics);
        dirtyphonics.addLink("www.facebook.com/dirtyphonics");
        dirtyphonics.addLink("www.youtube.com/user/dirtyphonics");
        dirtyphonics.addLink("www.twitter.com/dirtyphonics");
        dirtyphonics.addLink("www.soundcloud.com/dirtyphonics");
        dirtyphonics.addLink("www.deezer.com/artist/407883");
        dirtyphonics.setHeadliningScrore(resources.getInteger(R.integer.dirtyphonicsHeadliningScrore));
        bands.addBand(dirtyphonics);
        lineUp.addLineUpItem(new LineUpItem(dirtyphonics, mainStage, getDay(17, 1, 20)));

        Band fakear = new Band(i++, resources.getString(R.string.fakear), resources.getString(R.string.fakearDescription), resources.getString(R.string.world), resources.getString(R.string.france), R.drawable.artist_fakear);
        fakear.addLink("www.fakear.bandcamp.com/");
        fakear.addLink("www.facebook.com/fakear");
        fakear.addLink("www.soundcloud.com/fakear");
        fakear.addLink("play.spotify.com/artist/4eFImh8D3F15dtZk0JQlpT");
        fakear.addLink("www.deezer.com/artist/4777395");
        fakear.setHeadliningScrore(resources.getInteger(R.integer.fakearHeadliningScrore));
        bands.addBand(fakear);
        lineUp.addLineUpItem(new LineUpItem(fakear, mainStage, getDay(15, 23, 30)));

        Band theFatBadgers = new Band(i++, resources.getString(R.string.theFatBadgers), resources.getString(R.string.theFatBadgersDescription), resources.getString(R.string.funk), resources.getString(R.string.france), R.drawable.artist_the_fat_badgers);
        theFatBadgers.addLink("www.facebook.com/thefatbadgers");
        theFatBadgers.addLink("www.thefatbadgers.bandcamp.com/");
        theFatBadgers.addLink("www.youtube.com/channel/UCtjNzEU0zunAKUVsqZD-xgQ");
        theFatBadgers.addLink("www.soundcloud.com/the-fat-badgers");
        theFatBadgers.addLink("www.deezer.com/album/9228135");
        theFatBadgers.addLink("play.spotify.com/album/1ysv1ozHFdRQHuFPbyD5sc");
        theFatBadgers.setHeadliningScrore(resources.getInteger(R.integer.theFatBadgersHeadliningScrore));
        bands.addBand(theFatBadgers);
        lineUp.addLineUpItem(new LineUpItem(theFatBadgers, mainStage, getDay(14, 20, 30)));

        Band gutsLiveBand = new Band(i++, resources.getString(R.string.gutsLiveBand), resources.getString(R.string.gutsLiveBandDescription), resources.getString(R.string.hiphop), resources.getString(R.string.france), R.drawable.artist_guts);
        gutsLiveBand.addLink("www.guts-puravida.bandcamp.com/");
        gutsLiveBand.addLink("www.facebook.com/GutsOfficial");
        gutsLiveBand.addLink("www.soundcloud.com/guts");
        gutsLiveBand.addLink("www.deezer.com/artist/17262");
        gutsLiveBand.addLink("play.spotify.com/artist/5mMkUZv8uUrlH0SHX89BeS");
        gutsLiveBand.setHeadliningScrore(resources.getInteger(R.integer.gutsLiveBandHeadliningScrore));
        bands.addBand(gutsLiveBand);
        lineUp.addLineUpItem(new LineUpItem(gutsLiveBand, mainStage, getDay(14, 23, 30)));

        Band lyricson = new Band(i++, resources.getString(R.string.lyricson), resources.getString(R.string.lyricsonDescription), resources.getString(R.string.reggae), resources.getString(R.string.france), R.drawable.artist_lyricson);
        lyricson.addLink("www.facebook.com/pages/LYRICSON-Officiel/175802320992");
        lyricson.addLink("www.deezer.com/artist/16799");
        lyricson.setHeadliningScrore(resources.getInteger(R.integer.lyricsonHeadliningScrore));
        bands.addBand(lyricson);
        lineUp.addLineUpItem(new LineUpItem(lyricson, mainStage, getDay(14, 22, 00)));

        Band nto = new Band(i++, resources.getString(R.string.nto), resources.getString(R.string.ntoDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_nto);
        nto.addLink("www.facebook.com/nto.music");
        nto.addLink("www.deezer.com/artist/171883");
        nto.addLink("play.spotify.com/artist/7ry8L53T4oJtSIogGYuioq");
        nto.addLink("www.soundcloud.com/ntonto");
        nto.addLink("www.youtube.com/user/NtoTunes");
        nto.setHeadliningScrore(resources.getInteger(R.integer.ntoHeadliningScrore));
        bands.addBand(nto);
        lineUp.addLineUpItem(new LineUpItem(nto, mainStage, getDay(16, 2, 30)));

        Band poldoore = new Band(i++, resources.getString(R.string.poldoore), resources.getString(R.string.poldooreDescription), resources.getString(R.string.electro), resources.getString(R.string.belgique), R.drawable.artist_poldoore);
        poldoore.addLink("www.facebook.com/poldoore");
        poldoore.addLink("www.soundcloud.com/poldoore");
        poldoore.addLink("www.twitter.com/Poldoore");
        poldoore.addLink("www.youtube.com/user/PoldooreMusic");
        poldoore.addLink("www.deezer.com/artist/2306281");
        poldoore.addLink("play.spotify.com/artist/3ph6BKBPsjP7Vhtd1IXhkc");
        poldoore.setHeadliningScrore(resources.getInteger(R.integer.poldooreHeadliningScrore));
        bands.addBand(poldoore);
        lineUp.addLineUpItem(new LineUpItem(poldoore, mainStage, getDay(15, 20, 30)));

        Band salutCestCool = new Band(i++, resources.getString(R.string.salutCestCool), resources.getString(R.string.salutCestCoolDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_salut_cest_cool);
        salutCestCool.addLink("www.salutcestcool.com/");
        salutCestCool.addLink("www.facebook.com/salutcestcool");
        salutCestCool.addLink("www.deezer.com/artist/7802522");
        salutCestCool.addLink("www.twitter.com/salutcestcool");
        salutCestCool.setHeadliningScrore(resources.getInteger(R.integer.salutCestCoolHeadliningScrore));
        bands.addBand(salutCestCool);
        lineUp.addLineUpItem(new LineUpItem(salutCestCool, mainStage, getDay(15, 22, 00)));

        Band theGeeXVrv = new Band(i++, resources.getString(R.string.theGeeXVrv), resources.getString(R.string.theGeeXVrvDescription), resources.getString(R.string.hiphop), resources.getString(R.string.france), R.drawable.artist_geekvrv);
        theGeeXVrv.addLink("www.facebook.com/thegeekxvrv");
        theGeeXVrv.addLink("www.soundcloud.com/thegeek");
        theGeeXVrv.addLink("www.twitter.com/thegeekxvrv");
        theGeeXVrv.addLink("www.youtube.com/channel/UCmwye08VVAtOUcmdfccfd0A");
        theGeeXVrv.addLink("www.deezer.com/artist/5452854");
        theGeeXVrv.addLink("play.spotify.com/artist/4JhjlqgMbd4RlrT81VoTIF");
        theGeeXVrv.setHeadliningScrore(resources.getInteger(R.integer.theGeeXVrvHeadliningScrore));
        bands.addBand(theGeeXVrv);
        lineUp.addLineUpItem(new LineUpItem(theGeeXVrv, mainStage, getDay(16, 20, 00)));

        Band theNoisyFreaks = new Band(i++, resources.getString(R.string.theNoisyFreaks), resources.getString(R.string.theNoisyFreaksDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_the_noisy_freaks);
        theNoisyFreaks.addLink("www.thenoisyfreaks.net/");
        theNoisyFreaks.addLink("www.facebook.com/thenoisyfreaks");
        theNoisyFreaks.addLink("www.twitter.com/thenoisyfreaks");
        theNoisyFreaks.addLink("www.deezer.com/artist/1048872");
        theNoisyFreaks.addLink("play.spotify.com/artist/60sodhodkZh9N9pMudJnfa");
        theNoisyFreaks.addLink("www.soundcloud.com/thenoisyfreaks");
        theNoisyFreaks.addLink("www.youtube.com/channel/UC3t2fapsf1LzpVVRjTgMydw");
        theNoisyFreaks.setHeadliningScrore(resources.getInteger(R.integer.theNoisyFreaksHeadliningScrore));
        bands.addBand(theNoisyFreaks);
        lineUp.addLineUpItem(new LineUpItem(theNoisyFreaks, mainStage, getDay(16, 21, 20)));

        Band thylacine = new Band(i++, resources.getString(R.string.thylacine), resources.getString(R.string.thylacineDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_thylacine);
        thylacine.addLink("www.intuitive-records.com/");
        thylacine.addLink("www.facebook.com/thylacine.w");
        thylacine.addLink("www.soundcloud.com/thylacinew ");
        thylacine.addLink("www.twitter.com/thylacine_music");
        thylacine.addLink("www.youtube.com/channel/UCz0GJVTr8FrGAT4vElreSOw");
        thylacine.addLink("www.deezer.com/artist/106574");
        thylacine.addLink("play.spotify.com/artist/5If5Tdg66Q5X3L57G7A6Pn");
        thylacine.setHeadliningScrore(resources.getInteger(R.integer.thylacineHeadliningScrore));
        bands.addBand(thylacine);
        lineUp.addLineUpItem(new LineUpItem(thylacine, mainStage, getDay(16, 1, 00)));

        Band uz = new Band(i++, resources.getString(R.string.uz), resources.getString(R.string.uzDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_uz);
        uz.addLink("www.facebook.com/UZBalltrap");
        uz.addLink("www.soundcloud.com/ball-trap-music");
        uz.addLink("www.twitter.com/BallTrapMusic");
        uz.addLink("www.youtube.com/user/UZmusik");
        uz.addLink("www.deezer.com/artist/4064570");
        uz.addLink("play.spotify.com/artist/2hndbDJCfHwD1o2ZTzKLMo");
        uz.setHeadliningScrore(resources.getInteger(R.integer.uzHeadliningScrore));
        bands.addBand(uz);
        lineUp.addLineUpItem(new LineUpItem(uz, mainStage, getDay(17, 2, 40)));

        // Second stage

        Stage secondStage = new Stage(resources.getString(R.string.secondaryStage));
        secondStage.setScore(resources.getInteger(R.integer.secondaryStageScore));
        model.setSecondStage(secondStage);

        Band abaShantiItes = new Band(i++, resources.getString(R.string.abaShantiItes), resources.getString(R.string.abaShantiItesDescription), resources.getString(R.string.reggae), resources.getString(R.string.uk), R.drawable.artist_aba_shanti_ites);
        abaShantiItes.addLink("www.facebook.com/abashantiofficial");
        abaShantiItes.setHeadliningScrore(resources.getInteger(R.integer.abaShantiItesHeadliningScrore));
        bands.addBand(abaShantiItes);
        lineUp.addLineUpItem(new LineUpItem(abaShantiItes, secondStage, getDay(14, 21, 00)));

        Band blutch = new Band(i++, resources.getString(R.string.blutch), resources.getString(R.string.blutchDescription), resources.getString(R.string.hiphop), resources.getString(R.string.france), R.drawable.artist_blutch);
        blutch.addLink("www.facebook.com/pages/Blutch/333167113396807");
        blutch.addLink("www.soundcloud.com/blutch");
        blutch.addLink("www.youtube.com/user/Blutchmusic");
        blutch.addLink("www.twitter.com/Blutchmusic");
        blutch.setHeadliningScrore(resources.getInteger(R.integer.blutchHeadliningScrore));
        bands.addBand(blutch);
        lineUp.addLineUpItem(new LineUpItem(blutch, secondStage, getDay(15, 21, 00)));

        Band iSkankersHIFI = new Band(i++, resources.getString(R.string.iSkankersHIFI), resources.getString(R.string.iSkankersHIFIDescription), resources.getString(R.string.dubstep), resources.getString(R.string.france), R.drawable.artist_i_skankers_hifi);
        iSkankersHIFI.addLink("www.iskankers.com/");
        iSkankersHIFI.addLink("www.facebook.com/pages/International-Skankers-Hi-Fi/199809192159");
        iSkankersHIFI.addLink("www.soundcloud.com/iskankers");
        iSkankersHIFI.addLink("www.youtube.com/user/iskankers");
        iSkankersHIFI.setHeadliningScrore(resources.getInteger(R.integer.iSkankersHIFIHeadliningScrore));
        bands.addBand(iSkankersHIFI);
        lineUp.addLineUpItem(new LineUpItem(iSkankersHIFI, secondStage, getDay(14, 21, 00)));

        Band jamestarba = new Band(i++, resources.getString(R.string.jamestarba), resources.getString(R.string.jamestarbaDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_james_tarba);
        jamestarba.addLink("www.facebook.com/thejamestarba");
        jamestarba.addLink("www.soundcloud.com/jamestarba");
        jamestarba.addLink("www.twitter.com/JamesTarba");
        jamestarba.setHeadliningScrore(resources.getInteger(R.integer.jamestarbaHeadliningScrore));
        bands.addBand(jamestarba);
        lineUp.addLineUpItem(new LineUpItem(jamestarba, secondStage, getDay(16, 21, 00)));

        Band maximeDangles = new Band(i++, resources.getString(R.string.maximeDangles), resources.getString(R.string.maximeDanglesDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_maxime_dangles);
        maximeDangles.addLink("www.maximedangles.com");
        maximeDangles.addLink("www.facebook.com/maximedangles");
        maximeDangles.addLink("www.soundcloud.com/danglesmaxime");
        maximeDangles.addLink("www.twitter.com/maximedangles");
        maximeDangles.addLink("www.youtube.com/user/SIGNINDUSTRY");
        maximeDangles.addLink("www.deezer.com/artist/165526");
        maximeDangles.addLink("play.spotify.com/artist/3bJTyt4s2t9txeoMbzrLWk");
        maximeDangles.setHeadliningScrore(resources.getInteger(R.integer.maximeDanglesHeadliningScrore));
        bands.addBand(maximeDangles);
        lineUp.addLineUpItem(new LineUpItem(maximeDangles, secondStage, getDay(15, 21, 00)));

        Band oniris = new Band(i++, resources.getString(R.string.oniris), resources.getString(R.string.onirisDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_oniris);
        oniris.addLink("www.facebook.com/onirisdj");
        oniris.addLink("www.soundcloud.com/oniris");
        oniris.addLink("www.youtube.com/user/OnirisDj");
        oniris.addLink("www.twitter.com/OnirisDj");
        oniris.addLink("www.deezer.com/artist/4934073");
        oniris.setHeadliningScrore(resources.getInteger(R.integer.onirisHeadliningScrore));
        bands.addBand(oniris);
        lineUp.addLineUpItem(new LineUpItem(oniris, secondStage, getDay(15, 21, 00)));

        Band signalST = new Band(i++, resources.getString(R.string.signalST), resources.getString(R.string.signalSTDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_signal_st);
        signalST.addLink("www.facebook.com/signalst");
        signalST.addLink("www.soundcloud.com/signal-st");
        signalST.setHeadliningScrore(resources.getInteger(R.integer.signalSTHeadliningScrore));
        bands.addBand(signalST);
        lineUp.addLineUpItem(new LineUpItem(signalST, secondStage, getDay(16, 21, 00)));

        Band sonicCrew = new Band(i++, resources.getString(R.string.sonicCrew), resources.getString(R.string.sonicCrewDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_sonic_crew);
        sonicCrew.addLink("www.facebook.com/pages/Sonic-Crew/116726402811");
        sonicCrew.addLink("www.soundcloud.com/sonic-crew");
        sonicCrew.setHeadliningScrore(resources.getInteger(R.integer.sonicCrewHeadliningScrore));
        bands.addBand(sonicCrew);
        lineUp.addLineUpItem(new LineUpItem(sonicCrew, secondStage, getDay(15, 21, 00)));

        Band voiron = new Band(i++, resources.getString(R.string.voiron), resources.getString(R.string.voironDescription), resources.getString(R.string.electro), resources.getString(R.string.france), R.drawable.artist_voiron);
        voiron.addLink("www.facebook.com/pages/VOIRON/160864057405666");
        voiron.addLink("www.soundcloud.com/voiron");
        voiron.addLink("www.twitter.com/voiron_http");
        voiron.setHeadliningScrore(resources.getInteger(R.integer.voironHeadliningScrore));
        bands.addBand(voiron);
        lineUp.addLineUpItem(new LineUpItem(voiron, secondStage, getDay(16, 21, 00)));

        // Fest-Noz

        Stage festNozStage = new Stage(resources.getString(R.string.festNoz));
        festNozStage.setScore(resources.getInteger(R.integer.festNozScore));
        model.setThirdStage(festNozStage);

        Band beatBouetTrio = new Band(i++, resources.getString(R.string.beatBouetTrio), resources.getString(R.string.beatBouetTrioDescription), resources.getString(R.string.celtic), resources.getString(R.string.france), R.drawable.artist_beat_bouet_trio);
        beatBouetTrio.addLink("www.beatbouettrio.wix.com/beatbouettrio");
        beatBouetTrio.addLink("www.facebook.com/beatbouettrio");
        beatBouetTrio.addLink("www.deezer.com/artist/6927593");
        beatBouetTrio.setHeadliningScrore(resources.getInteger(R.integer.beatBouetTrioHeadliningScrore));
        bands.addBand(beatBouetTrio);
        lineUp.addLineUpItem(new LineUpItem(beatBouetTrio, festNozStage, getDay(13, 21, 00)));

        Band duoBlainLeyzour = new Band(i++, resources.getString(R.string.duoBlainLeyzour), resources.getString(R.string.duoBlainLeyzourDescription), resources.getString(R.string.celtic), resources.getString(R.string.france), R.drawable.artist_duo_blain_leyzour);
        duoBlainLeyzour.addLink("www.blain-leyzour.com/");
        duoBlainLeyzour.addLink("www.facebook.com/duo.blain.leyzour");
        duoBlainLeyzour.setHeadliningScrore(resources.getInteger(R.integer.duoBlainLeyzourHeadliningScrore));
        bands.addBand(duoBlainLeyzour);
        lineUp.addLineUpItem(new LineUpItem(duoBlainLeyzour, festNozStage, getDay(13, 21, 00)));

        Band duoJossetMartin = new Band(i++, resources.getString(R.string.duoJossetMartin), resources.getString(R.string.duoJossetMartinDescription), resources.getString(R.string.celtic), resources.getString(R.string.france), R.drawable.artist_duo_josset_martin);
        duoJossetMartin.setHeadliningScrore(resources.getInteger(R.integer.duoJossetMartinHeadliningScrore));
        bands.addBand(duoJossetMartin);
        lineUp.addLineUpItem(new LineUpItem(duoJossetMartin, festNozStage, getDay(13, 21, 00)));

        Band kendirvi = new Band(i++, resources.getString(R.string.kendirvi), resources.getString(R.string.kendirviDescription), resources.getString(R.string.celtic), resources.getString(R.string.france), R.drawable.artist_kendirvi);
        kendirvi.addLink("www.kendirvi.fr/");
        kendirvi.addLink("www.facebook.com/Kendirvi");
        kendirvi.addLink("www.youtube.com/channel/UC_mBI6EH1DaLcD7lMfCO63Q");
        kendirvi.addLink("www.soundcloud.com/kendirvi");
        kendirvi.addLink("www.twitter.com/kendirvi");
        kendirvi.setHeadliningScrore(resources.getInteger(R.integer.kendirviHeadliningScrore));
        bands.addBand(kendirvi);
        lineUp.addLineUpItem(new LineUpItem(kendirvi, festNozStage, getDay(13, 21, 00)));
    }

    private static void loadSolex(RnsModel model, Resources resources) {
        int i = 0;
        Races races = model.getRaces();

        Race technique = new Race(i++, resources.getString(R.string.technical), getDay(15, 12, 00), resources.getString(R.string.technicalInfos));
        races.addRace(technique);

        Race endurance = new Race(i++, resources.getString(R.string.endurance), getDay(16, 12, 00), resources.getString(R.string.enduranceInfos), R.drawable.endurance_2015);
        races.addRace(endurance);

        Race vitesse = new Race(i++, resources.getString(R.string.speed), getDay(17, 12, 00), resources.getString(R.string.speedInfos), R.drawable.vitesse_2015);
        races.addRace(vitesse);
    }

    protected static Calendar getDay(int date, int hour, int minute) {
        Calendar day = Calendar.getInstance();
        day.set(Calendar.DAY_OF_MONTH, date);
        day.set(Calendar.MONTH, Calendar.MAY);
        day.set(Calendar.YEAR, 2015);
        day.set(Calendar.HOUR_OF_DAY, hour);
        day.set(Calendar.MINUTE, minute);
        return day;
    }
}
