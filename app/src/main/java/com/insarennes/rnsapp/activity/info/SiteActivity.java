package com.insarennes.rnsapp.activity.info;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;

public class SiteActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_site);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.site));

        TextView disabledTitle = (TextView) findViewById(R.id.site_disabled_title);
        TextView accessTitle = (TextView) findViewById(R.id.site_access_title);
        TextView securityTitle = (TextView) findViewById(R.id.site_security_title);
        TextView paymentTitle = (TextView) findViewById(R.id.site_payment_title);
        TextView preventionTitle = (TextView) findViewById(R.id.site_prevention_title);

        Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(this, CustomFontsLoader.ARCA_HEAVY);
        disabledTitle.setTypeface(arcaHeavyFont);
        accessTitle.setTypeface(arcaHeavyFont);
        securityTitle.setTypeface(arcaHeavyFont);
        paymentTitle.setTypeface(arcaHeavyFont);
        preventionTitle.setTypeface(arcaHeavyFont);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
        }
        return true;
    }
}

