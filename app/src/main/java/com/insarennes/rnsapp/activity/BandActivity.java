package com.insarennes.rnsapp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;
import com.insarennes.rnsapp.model.Band;
import com.insarennes.rnsapp.model.LineUpItem;
import com.insarennes.rnsapp.model.Link;
import com.insarennes.rnsapp.model.RnsModel;
import com.insarennes.rnsapp.view.LinkListViewHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BandActivity extends ActionBarActivity {

    public static final String BAND_ID_KEY = "BandID";
    public static final int OPEN_LINEUP_REQUESTCODE = 0;

    protected int mBandId;
    protected Band mBand;
    protected LineUpItem mLineUp;

    protected View mTitle;
    protected View mHeader;

    protected Boolean mAnimationDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_band);

        mBandId = getIntent().getExtras().getInt(BAND_ID_KEY);
        Band currentBand = RnsModel.getInstance().getBands().getBandFromId(mBandId);
        mLineUp = Iterables.getFirst(RnsModel.getInstance().getLineUp().getLineUpItems(currentBand), null);

        mAnimationDone = false;
        mTitle = findViewById(R.id.band_title_layout);
        mHeader = findViewById(R.id.band_header_layout);

        ImageView preview = (ImageView) findViewById(R.id.band_preview);
        TextView name = (TextView) findViewById(R.id.band_name);
        TextView genre = (TextView) findViewById(R.id.band_genre);
        TextView country = (TextView) findViewById(R.id.band_country);
        Button day = (Button) findViewById(R.id.band_date_button);
        TextView stage = (TextView) findViewById(R.id.band_stage);
        TextView descrption = (TextView) findViewById(R.id.band_description);
        LinearLayout links = (LinearLayout) findViewById(R.id.band_links);

        setTitle(currentBand.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface boomtownDecoFont = CustomFontsLoader.getTypeface(this, CustomFontsLoader.BOOMTOWN_DECO);
        name.setTypeface(boomtownDecoFont);

        Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(this, CustomFontsLoader.ARCA_HEAVY);
        genre.setTypeface(arcaHeavyFont);
        country.setTypeface(arcaHeavyFont);
        day.setTypeface(arcaHeavyFont);

        preview.setImageResource(currentBand.getImageResId());
        name.setText(currentBand.getName());
        genre.setText(currentBand.getGenre());
        country.setText(currentBand.getCountry());

        if (mLineUp != null) {
            day.setText(getDay(mLineUp.getDay()));
            stage.setText(mLineUp.getStage().getName());
        }

        descrption.setText(currentBand.getDescription());

        List<Link> orderedLinks = Lists.newArrayList(currentBand.getLinks());
        Collections.sort(orderedLinks, new LinkComparator());

        LayoutInflater inflater = LayoutInflater.from(this);
        LinkListViewHelper linkListViewHelper = new LinkListViewHelper(getResources());
        linkListViewHelper.addLinks(inflater, links, orderedLinks);
    }

    public void openLineUp(View view) {
        if(mLineUp != null && mLineUp.getDay().get(Calendar.DAY_OF_MONTH) != 13) {
            Intent intent = new Intent();
            intent.putExtra(BAND_ID_KEY, mBandId);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus && !mAnimationDone) {
            mTitle.startAnimation(AnimationUtils.loadAnimation(BandActivity.this, R.anim.slide_in_top));
            mHeader.startAnimation(AnimationUtils.loadAnimation(BandActivity.this, R.anim.fade_in));
            mAnimationDone = true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
        }
        return true;
    }

    protected String getDay(Calendar day) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        return sdf.format(day.getTime()).toUpperCase() + " " + day.get(Calendar.DAY_OF_MONTH);
    }

    protected class LinkComparator implements Comparator<Link> {

        @Override
        public int compare(Link link1, Link link2) {
            ArrayList<Link.LinkType> order = new ArrayList<>();

            order.add(Link.LinkType.OFFICIAL);
            order.add(Link.LinkType.SOUNDCLOUD);
            order.add(Link.LinkType.SPOTIFY);
            order.add(Link.LinkType.DEEZER);
            order.add(Link.LinkType.YOUTUBE);
            order.add(Link.LinkType.FACEBOOK);
            order.add(Link.LinkType.TWITTER);

            int index1 = order.indexOf(link1.getType());
            int index2 = order.indexOf(link2.getType());

            return index1 - index2;
        }

    }
}
