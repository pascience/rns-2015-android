package com.insarennes.rnsapp.activity.info;

import android.os.Bundle;

import com.insarennes.rnsapp.R;

public class WhereToSleepActivity extends AbstractInformationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getResources().getString(R.string.whereToSleep));

        addText(R.string.whereToSleepInfos);
    }
}

