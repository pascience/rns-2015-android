package com.insarennes.rnsapp.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;
import com.insarennes.rnsapp.model.LineUpItem;
import com.insarennes.rnsapp.model.Race;
import com.insarennes.rnsapp.model.RnsModel;
import com.insarennes.rnsapp.view.AnimatedLayout;
import com.insarennes.rnsapp.view.CountDownView;
import com.insarennes.rnsapp.view.ImageLoader;

import java.util.Calendar;

public class HomeFragment extends Fragment {

    protected View mView;

    protected CountDownView mCountDownView;
    protected View mBandsButton;
    protected AnimatedLayout mBandsLayout;
    protected View mRacesButton;
    protected AnimatedLayout mRacesLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Calendar now = RnsModel.getInstance().now();
        if (now.before(RnsModel.getInstance().getFestivalBeginning())) {
            return inflatePreFestival(inflater, container);
        } else if (now.after(RnsModel.getInstance().getFestivalEnd())) {
            return inflatePostFestival(inflater, container);
        } else {
            return inflateFestival(inflater, container);
        }
    }

    public View inflatePreFestival(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        mCountDownView = (CountDownView) mView.findViewById(R.id.home_countdown);
        mCountDownView.setDeadLine(RnsModel.getInstance().getFestivalBeginning());

        return mView;
    }

    public View inflateFestival(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        mCountDownView = (CountDownView) mView.findViewById(R.id.home_countdown);
        mCountDownView.setVisibility(View.GONE);

        mBandsButton = mView.findViewById(R.id.home_bands_button);
        mBandsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = RnsModel.getInstance().now();
                getMainActivity().showLineUp(now.get(Calendar.DAY_OF_MONTH));
            }
        });

        mBandsLayout = (AnimatedLayout) mBandsButton.findViewById(R.id.home_item_contents);

        mRacesButton = mView.findViewById(R.id.home_races_button);
        mRacesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().showRaces();
            }
        });

        mRacesLayout = (AnimatedLayout) mRacesButton.findViewById(R.id.home_item_contents);
        mRacesLayout.setDelayTime(2000);

        return mView;
    }

    public View inflatePostFestival(LayoutInflater inflater, ViewGroup container) {
        return inflateFestival(inflater, container);
    }

    public MainActivity getMainActivity() {
        return (MainActivity)getActivity();
    }

    protected void loadBands() {
        Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(getActivity(), CustomFontsLoader.ARCA_HEAVY);

        ImageView bandsImage = (ImageView) mBandsButton.findViewById(R.id.home_item_image);
        ImageLoader.loadImage(getActivity(), getResources(), bandsImage, R.drawable.guitar, 150, 150);

        View bandsTitle = mView.inflate(getActivity(), R.layout.adapter_home_content, null);
        TextView bandsContent = (TextView) bandsTitle.findViewById(R.id.home_item_content);

        bandsContent.setText(R.string.gigs);
        bandsContent.setTypeface(arcaHeavyFont);

        mBandsLayout.addAnimatedView(bandsTitle);

        Calendar now = RnsModel.getInstance().now();
        if (!now.after(RnsModel.getInstance().getFestivalEnd())) {
            LineUpItem currentBand = RnsModel.getInstance().getLineUp().getCurrentLineUpItem(RnsModel.getInstance().getMainStage());
            LineUpItem nextBand = RnsModel.getInstance().getLineUp().getNextLineUpItem(RnsModel.getInstance().getMainStage());

            if (currentBand != null) {
                View currentBandView = mView.inflate(getActivity(), R.layout.adapter_home_content, null);
                TextView currentBandHeader = (TextView) currentBandView.findViewById(R.id.home_item_header);
                TextView currentBandContent = (TextView) currentBandView.findViewById(R.id.home_item_content);

                currentBandHeader.setText(R.string.nowPlaying);
                currentBandContent.setText(currentBand.getBand().getName());
                currentBandContent.setTypeface(arcaHeavyFont);

                mBandsLayout.addAnimatedView(currentBandView);
            }

            if (nextBand != null) {
                View nextBandView = mView.inflate(getActivity(), R.layout.adapter_home_content, null);
                TextView nextBandHeader = (TextView) nextBandView.findViewById(R.id.home_item_header);
                TextView nextBandContent = (TextView) nextBandView.findViewById(R.id.home_item_content);

                nextBandHeader.setText(R.string.nextPlaying);
                nextBandContent.setText(nextBand.getBand().getName());
                nextBandContent.setTypeface(arcaHeavyFont);

                mBandsLayout.addAnimatedView(nextBandView);
            }
        }
    }

    protected void loadRaces() {

        Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(getActivity(), CustomFontsLoader.ARCA_HEAVY);

        ImageView racesImage = (ImageView) mRacesButton.findViewById(R.id.home_item_image);
        ImageLoader.loadImage(getActivity(), getResources(), racesImage, R.drawable.solex, 150, 150);

        View racesTitle = mView.inflate(getActivity(), R.layout.adapter_home_content, null);
        TextView racesContent = (TextView) racesTitle.findViewById(R.id.home_item_content);

        racesContent.setText(R.string.races);
        racesContent.setTypeface(arcaHeavyFont);

        mRacesLayout.addAnimatedView(racesTitle);

        Calendar now = RnsModel.getInstance().now();
        if (!now.after(RnsModel.getInstance().getFestivalEnd())) {
            Race currentRace = RnsModel.getInstance().getRaces().getCurrentRace();

            if (currentRace != null) {
                View currentRaceView = mView.inflate(getActivity(), R.layout.adapter_home_content, null);
                TextView currentRaceHeader = (TextView) currentRaceView.findViewById(R.id.home_item_header);
                TextView currentRaceContent = (TextView) currentRaceView.findViewById(R.id.home_item_content);

                currentRaceHeader.setText(R.string.nowPlaying);
                currentRaceContent.setText(currentRace.getName());
                currentRaceContent.setTypeface(arcaHeavyFont);

                mRacesLayout.addAnimatedView(currentRaceView);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Calendar now = RnsModel.getInstance().now();
        if (now.before(RnsModel.getInstance().getFestivalBeginning())) {
            mCountDownView.startCountdown();
        }
        mBandsLayout.removeAllAnimatedViews();
        loadBands();
        mBandsLayout.startSwitch();
        mRacesLayout.removeAllAnimatedViews();
        loadRaces();
        mRacesLayout.startSwitch();
    }

    @Override
    public void onPause() {
        super.onPause();
        Calendar now = RnsModel.getInstance().now();
        if (now.before(RnsModel.getInstance().getFestivalBeginning())) {
            mCountDownView.stopCountdown();
        }
        mBandsLayout.stopSwitch();
        mRacesLayout.stopSwitch();
    }
}
