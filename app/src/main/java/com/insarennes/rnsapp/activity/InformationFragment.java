package com.insarennes.rnsapp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.activity.info.ContactsActivity;
import com.insarennes.rnsapp.activity.info.FestivalActivity;
import com.insarennes.rnsapp.activity.info.HowToGetHereActivity;
import com.insarennes.rnsapp.activity.info.PraticalInformationActivity;
import com.insarennes.rnsapp.activity.info.SiteActivity;
import com.insarennes.rnsapp.activity.info.WhereToSleepActivity;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;

public class InformationFragment extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information, container, false);

        Typeface arcaHeavyFont = CustomFontsLoader.getTypeface(getActivity(), CustomFontsLoader.ARCA_HEAVY);

        Button contactsButton = (Button) view.findViewById(R.id.contactsButton);
        contactsButton.setOnClickListener(this);
        contactsButton.setTypeface(arcaHeavyFont);
        Button ticketsButton = (Button) view.findViewById(R.id.ticketsButton);
        ticketsButton.setOnClickListener(this);
        ticketsButton.setTypeface(arcaHeavyFont);
        Button festivalButton = (Button) view.findViewById(R.id.festivalButton);
        festivalButton.setOnClickListener(this);
        festivalButton.setTypeface(arcaHeavyFont);
        Button infosButton = (Button) view.findViewById(R.id.infosButton);
        infosButton.setOnClickListener(this);
        infosButton.setTypeface(arcaHeavyFont);
        Button howToGetHereButton = (Button) view.findViewById(R.id.howToGetHereButton);
        howToGetHereButton.setOnClickListener(this);
        howToGetHereButton.setTypeface(arcaHeavyFont);
        Button whereToSleepButton = (Button) view.findViewById(R.id.whereToSleepButton);
        whereToSleepButton.setOnClickListener(this);
        whereToSleepButton.setTypeface(arcaHeavyFont);
        Button sponsorsButton = (Button) view.findViewById(R.id.sponsorsButton);
        sponsorsButton.setOnClickListener(this);
        sponsorsButton.setTypeface(arcaHeavyFont);
        Button siteButton = (Button) view.findViewById(R.id.siteButton);
        siteButton.setOnClickListener(this);
        siteButton.setTypeface(arcaHeavyFont);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contactsButton:
                openContacts();
                break;
            case R.id.ticketsButton:
                openTickets();
                break;
            case R.id.festivalButton:
                openFestival();
                break;
            case R.id.infosButton:
                openInfos();
                break;
            case R.id.howToGetHereButton:
                openHowToGetHere();
                break;
            case R.id.whereToSleepButton:
                openWhereToSleep();
                break;
            case R.id.sponsorsButton:
                openSponsors();
                break;
            case R.id.siteButton:
                openSite();
                break;
        }
    }

    public void openContacts() {
        Intent intent = new Intent(getActivity(), ContactsActivity.class);
        startActivity(intent);
    }

    public void openTickets() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.digitick.com/index-css5-rocknsolex-pg1.html"));
        startActivity(browserIntent);
    }

    public void openFestival() {
        Intent intent = new Intent(getActivity(), FestivalActivity.class);
        startActivity(intent);
    }

    public void openInfos() {
        Intent intent = new Intent(getActivity(), PraticalInformationActivity.class);
        startActivity(intent);
    }

    public void openHowToGetHere() {
        Intent intent = new Intent(getActivity(), HowToGetHereActivity.class);
        startActivity(intent);
    }

    public void openWhereToSleep() {
        Intent intent = new Intent(getActivity(), WhereToSleepActivity.class);
        startActivity(intent);
    }

    public void openSponsors() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.rocknsolex.fr/festival.html#partenaires"));
        startActivity(browserIntent);
    }

    public void openSite() {
        Intent intent = new Intent(getActivity(), SiteActivity.class);
        startActivity(intent);
    }

}
