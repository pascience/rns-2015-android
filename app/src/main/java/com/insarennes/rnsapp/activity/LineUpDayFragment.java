package com.insarennes.rnsapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.insarennes.rnsapp.R;
import com.insarennes.rnsapp.comparator.LineUpItemComparator;
import com.insarennes.rnsapp.fonts.CustomFontsLoader;
import com.insarennes.rnsapp.model.LineUpItem;
import com.insarennes.rnsapp.model.RnsModel;
import com.insarennes.rnsapp.model.Stage;
import com.insarennes.rnsapp.view.ImageLoader;
import com.koushikdutta.ion.Ion;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class LineUpDayFragment extends ListFragment {

    private static final String ARG_DAY = "day";
    private int mDay;

    private ArrayAdapter<Object> mAdapter;

    public static LineUpDayFragment newInstance(int day) {
        LineUpDayFragment fragment = new LineUpDayFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_DAY, day);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDay = getArguments().getInt(ARG_DAY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_line_up_day, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initListAdapter();
    }

    private void initListAdapter() {
        ArrayList<Object> categorizedList = new ArrayList<>();
        Map<Stage, Collection<LineUpItem>> map = RnsModel.getInstance().getLineUp().getCategorizedLineUpItems(mDay);

        for (Map.Entry<Stage, Collection<LineUpItem>> entry : map.entrySet()) {
            LineUpCategory luc = new LineUpCategory();
            luc.name = entry.getKey().getName();
            categorizedList.add(luc); // category
            List<LineUpItem> lineUpItems = Lists.newArrayList(entry.getValue());
            Collections.sort(lineUpItems, new LineUpItemComparator());
            categorizedList.addAll(lineUpItems); // line up items
        }

        Object[] array = categorizedList.toArray(new Object[categorizedList.size()]);
        mAdapter = new LineUpDayAdapter(getActivity(), array);
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Object objClicked = mAdapter.getItem(position);
        if (objClicked instanceof LineUpItem) {
            // Get LineUpItem clicked :
            LineUpItem item = (LineUpItem) mAdapter.getItem(position);
            // Start band activity :
            Intent i = new Intent(getActivity(), BandActivity.class);
            i.putExtra(BandActivity.BAND_ID_KEY, item.getBand().getId());
            startActivity(i);
        }
    }

    public class LineUpCategory {
        public String name;
    }

    public class LineUpDayAdapter extends ArrayAdapter<Object> {
        private final Context context;
        private final Object[] values;

        public LineUpDayAdapter(Context context, Object[] values) {
            super(context, R.layout.adapter_line_up, values);
            this.context = context;
            this.values = values;
        }

        // TODO : reuse existing ViewHolder when scrolling for better performance (by changing text and image instead of create a new view)

        /**
         * Return the artist lineup row or a category row
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView;

            // - LineUp row -
            if (values[position] instanceof LineUpItem) {
                LineUpItem lineUpItem = (LineUpItem) values[position];
                rowView = inflater.inflate(R.layout.adapter_line_up, parent, false);

                // Text :
                TextView textView = (TextView) rowView.findViewById(R.id.text);
                textView.setText(lineUpItem.getBand().getName());
                textView.setTypeface(CustomFontsLoader.getTypeface(getContext(), CustomFontsLoader.ARCA_HEAVY));

                // Image :
                ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
                int imageResId = lineUpItem.getBand().getImageResId();
                ImageLoader.loadImage(getActivity(), getResources(), imageView, imageResId, 150, 150);
            }
            // - Category row -
            else {
                LineUpCategory lineUpCategory = (LineUpCategory) values[position];
                rowView = inflater.inflate(R.layout.adapter_line_up_category, parent, false);
                TextView textView = (TextView) rowView.findViewById(R.id.text);
                textView.setText(stripAccents(lineUpCategory.name));
                textView.setTypeface(CustomFontsLoader.getTypeface(getContext(), CustomFontsLoader.BOOMTOWN_DECO));
            }
            return rowView;
        }

        public String stripAccents(String s) {
            s = Normalizer.normalize(s, Normalizer.Form.NFD);
            s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
            return s;
        }
    }
}
