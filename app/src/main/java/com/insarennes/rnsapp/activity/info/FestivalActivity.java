package com.insarennes.rnsapp.activity.info;

import android.os.Bundle;

import com.insarennes.rnsapp.R;

public class FestivalActivity extends AbstractInformationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getResources().getString(R.string.festival));

        addText(R.string.infosFestival1);
        addText(R.string.infosFestival2);
        addText(R.string.infosFestival3);
    }
}
