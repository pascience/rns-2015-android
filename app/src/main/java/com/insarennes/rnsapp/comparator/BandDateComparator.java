package com.insarennes.rnsapp.comparator;

import com.google.common.collect.Iterables;
import com.insarennes.rnsapp.model.Band;
import com.insarennes.rnsapp.model.LineUp;
import com.insarennes.rnsapp.model.LineUpItem;

import java.util.Comparator;

public final class BandDateComparator implements Comparator<Band> {

    private LineUp mLineUp;

    public BandDateComparator(LineUp lineUp) {
        this.mLineUp = lineUp;
    }

    @Override
    public int compare(Band band1, Band band2) {
        LineUpItem lineUpItem1 = Iterables.getFirst(mLineUp.getLineUpItems(band1), null);
        LineUpItem lineUpItem2 = Iterables.getFirst(mLineUp.getLineUpItems(band2), null);

        if (lineUpItem1 == null) {
            return 1;
        } else if (lineUpItem2 == null) {
            return -1;
        } else {
            return lineUpItem1.getDate().compareTo(lineUpItem2.getDate());
        }
    }
}
