package com.insarennes.rnsapp.comparator;

import com.insarennes.rnsapp.model.LineUpItem;

import java.util.Calendar;
import java.util.Comparator;

public class LineUpItemComparator implements Comparator<LineUpItem> {

    @Override
    public int compare(LineUpItem lineUp1, LineUpItem lineUp2) {
        Calendar date1 = (Calendar)lineUp1.getDate();
        Calendar date2 = (Calendar)lineUp2.getDate();
        return date1.compareTo(date2);
    }
}
